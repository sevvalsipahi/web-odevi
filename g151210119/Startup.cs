﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_15412.Startup))]
namespace _15412
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
